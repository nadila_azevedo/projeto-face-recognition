from cv2 import rectangle
import face_recognition
import numpy as np
import cv2
import os



def compare_photos(rgb_frame):
  

    face_locations = face_recognition.face_locations(rgb_frame)
    face_encodings = face_recognition.face_encodings(rgb_frame,face_locations)
  
    return face_locations, face_encodings
    
       
def find_result(result,encodings,unknown):
    list_encodings = list(encodings.values())
    face_distances = face_recognition.face_distance(list_encodings, unknown)
    names = list(encodings.keys())
    name_id = np.argmin(face_distances)
    if result[name_id]:
        nome = names[name_id]
        print(nome)
        return nome
    else:
        nome = "Desconhecido"
        return nome


    
def search():
    encodings = {}
    for foto in os.listdir('imagens'):
        foto_path = f'imagens/{foto}'
        name = foto.replace('.jpg','').replace('.png','').replace('jpeg','')
        picture = face_recognition.load_image_file(foto_path)
        encoding = face_recognition.face_encodings(picture)
        if(len(encoding[0]))>0:
            encodings[name] = encoding[0]

    return encodings 
 
 
vid =  cv2.VideoCapture(0)
encodings =  search()

while(True):
    ret, frame = vid.read()
    rgb_frame = frame[:, :, ::-1]
    
    face_locations,face_encodings = compare_photos(rgb_frame)
    for (top,right, bottom, left), face_encoding in zip(face_locations,face_encodings):
        matches = face_recognition.compare_faces(list(encodings.values()),face_encoding)
        green = (0, 255, 0)
        red = (0,0,255)
        font = cv2.FONT_HERSHEY_DUPLEX
        name = find_result(matches,encodings,face_encoding)
        #print(name)
        if name!='Desconhecido':
            cv2.rectangle(frame,(left,top),(right,bottom),green,2)

        else:
            cv2.rectangle(frame,(left,top),(right,bottom),red,2)
        cv2.putText(frame,name,(left+6,bottom-6),font,1.0,(255,255,255),1)
        cv2.imshow("", frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

vid.release()
cv2.destroyAllWindows()


